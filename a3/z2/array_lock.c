#include "lock.h"
#include "../common/alloc.h"

struct lock_struct {
	int size;
    int tail;
    unsigned char *flag;
};

// Thread Local Variable for each thread for storing slot safely
__thread int mySlotIndex;

lock_t *lock_init(int nthreads)
{
    int i;
	lock_t *lock;
	XMALLOC(lock, 1);
    lock->size = nthreads;
    lock->tail = 0;
    XMALLOC(lock->flag, lock->size);
    lock->flag[0] = 1; // 1 == TRUE
    for(i=1; i < lock->size; i++) lock->flag[i] = 0;
	return lock;
}

void lock_free(lock_t *lock)
{
    XFREE(lock->flag);
	XFREE(lock);
}

void lock_acquire(lock_t *lock)
{
    lock_t *l = lock;
    int slot = __sync_fetch_and_add(&l->tail, 1) % l->size;
    mySlotIndex = slot;
    while(!l->flag[slot]){};    
}

void lock_release(lock_t *lock)
{
    lock_t *l = lock;
    int slot = mySlotIndex;
    l->flag[slot] = 0;
    l->flag[(slot+1)%l->size] = 1;
}
