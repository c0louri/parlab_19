##ve the Job a descriptive name
#PBS -N run_z1_init

## Output and error files
#PBS -o logs/run_z1.out
#PBS -e logs/run_z1.err

## How many machines should we get? 
#PBS -l nodes=sandman:ppn=64

##How long should the job run for?
#PBS -l walltime=00:05:00

## Start 
## Run make in the src folder (modify properly)

cd /home/parallel/parlab10/a3/z1/
EX1="logs/z1_ex1_final"
EX2="logs/z1_ex2_final"

## 1 THREAD
MT_CONF=0 ./accounts > $EX1
MT_CONF=0 ./accounts > $EX2
## 2 THREADS
MT_CONF=0,1 ./accounts >> $EX1
MT_CONF=0,8 ./accounts >> $EX2
## 4 THREADS
MT_CONF=0,1,2,3 ./accounts >> $EX1
MT_CONF=0,8,16,24 ./accounts >> $EX2
## 8 THREADS
MT_CONF=0,1,2,3,4,5,6,7 ./accounts >> $EX1
MT_CONF=0,1,8,9,16,17,24,25 ./accounts >> $EX2
## 16 THREADS
MT_CONF=0,1,2,3,4,5,6,7,32,33,34,35,36,37,38,39 ./accounts >> $EX1
MT_CONF=0,1,2,3,8,9,10,11,16,17,18,19,24,25,26,27 ./accounts >> $EX2
## 32 THREADS 0-15,32-47
MT_CONF=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47 ./accounts >> $EX1
MT_CONF=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31 ./accounts >> $EX2
## 64 THREADS
MT_CONF=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63 ./accounts >> $EX1
MT_CONF=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63 ./accounts >> $EX2






