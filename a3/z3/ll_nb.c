#include <stdio.h>
#include <stdlib.h> /* rand() */
#include <limits.h>

#include "../common/alloc.h"
#include "ll.h"

typedef struct ll_node {
	int key;
	struct ll_node *next;
	/* other fields here? */
    //char padding[64 - sizeof(int) - sizeof(ll_node*)];
} ll_node_t;

struct linked_list {
	ll_node_t *head;
	/* other fields here? */
};

/**
 * Create a new linked list node.
 **/
static ll_node_t *ll_node_new(int key)
{
	ll_node_t *ret;

	XMALLOC(ret, 1);
	ret->key = key;
	ret->next = NULL;
	/* Other initializations here? */

	return ret;
}

/**
 * Free a linked list node.
 **/
static void ll_node_free(ll_node_t *ll_node)
{
	XFREE(ll_node);
}

/**
 * Create a new empty linked list.
 **/
ll_t *ll_new()
{
	ll_t *ret;

	XMALLOC(ret, 1);
	ret->head = ll_node_new(-1);
	ret->head->next = ll_node_new(INT_MAX);
	ret->head->next->next = NULL;

	return ret;
}

/**
 * Free a linked list and all its contained nodes.
 **/
void ll_free(ll_t *ll)
{
	ll_node_t *next, *curr = ll->head;
	while (curr) {
		next = curr->next;
		ll_node_free(curr);
		curr = next;
	}
	XFREE(ll);
}

typedef struct window {
    ll_node_t *curr, *next;
} window_t;

ll_node_t* get_reference(ll_node_t* node) {
    return (ll_node_t*) ((long long) node & (long long) ~1); 
}

ll_node_t* get(ll_node_t* node, int* marked) {
    *marked = (long long) node & 1;
    return get_reference(node);
}

// returns pointer 
window_t * find(ll_t *ll, int key) {
    ll_node_t *curr, *next, *succ;
    int *marked;
    int snip;
    window_t * ret;
    XMALLOC(ret,1);
    XMALLOC(marked,1);

    retry: while(1) {
        curr = ll->head;
        next = get_reference(curr->next);
        while(1) {
            succ = get(next->next,marked);
            while(*marked) {
                snip = __sync_bool_compare_and_swap(&(curr->next),next,succ);
                if (!snip)
                    goto retry;
                next = succ;
                suc = get(next->next,marked);
            }
            if (next->key >= key) {
                ret->curr = curr;
                ret->next = next;
                return ret;
            }
            curr = next;
            next = succ;
        }
    }
}

int ll_contains(ll_t *ll, int key)
{
    int *marked;
    //ll_node_t *curr, *succ;
    ll_node_t *curr;
    
    XMALLOC(marked,1);
    curr = get(ll->head,marked);    

    while (curr->key < key) {
        curr = get(curr->next,marked);
        //succ = get(curr->next,marked);
    }
    
    return curr->key == key && !(*marked);
}

int ll_add(ll_t *ll, int key)
{
    ll_node_t *curr, *next, *new_node;
    window_t *res;
    while (1) {
        res = find(ll,key);
        curr = res->curr;
        next = res->next;
        if (next->key == key)
            return 0;
        else {
            new_node = ll_node_new(key);
            new_node->next = next;
            if (__sync_bool_compare_and_swap(&(curr->next),curr->next,new_node))
                return 1;
        }
    }
}

int ll_remove(ll_t *ll, int key)
{
	int snip;
    ll_node_t *curr, *next, *succ, *temp;
    window_t *res;
    
    while (1) {
        res = find(ll, key);
        curr = res->curr;
        next = res->next;
        if (next->key != key)
            return 0;
        else {
            //succ = get_reference(next->next);
            succ = next->next;
            //instead of attempt_mark
            temp = (ll_node_t*) ((long long) next | 1);
            snip = __sync_bool_compare_and_swap(&(curr->next),curr->next,temp);
            //snip = attempt_mark(next->next,succ);
            if (!snip)
                continue;
            __sync_bool_compare_and_swap(&(curr->next),curr->next,succ);
            return 1;
        }
    }
}

/**
 * Print a linked list.
 **/
void ll_print(ll_t *ll)
{
	ll_node_t *curr = ll->head;
	printf("LIST [");
	while (curr) {
		if (curr->key == INT_MAX)
			printf(" -> MAX");
		else
			printf(" -> %d", curr->key);
		curr = curr->next;
	}
	printf(" ]\n");
}
