#include <stdio.h>
#include <stdlib.h> /* rand() */
#include <limits.h>
#include <pthread.h> /* for pthread_spinlock_t */

#include "../common/alloc.h"
#include "ll.h"
//#include "lock.h"

typedef struct ll_node {
	int key;
	struct ll_node *next;
    //spinlock for every node
    pthread_spinlock_t lock; 
} ll_node_t;

struct linked_list {
	ll_node_t *head;
	/* other fields here? */
};

/**
 * Create a new linked list node.
 **/
static ll_node_t *ll_node_new(int key)
{
	ll_node_t *ret;
    //printf("new node\n");
	XMALLOC(ret, 1);
	ret->key = key;
	ret->next = NULL;
    //initialize the lock
    //ret->lock = lock_init();
    pthread_spin_init(&(ret->lock), PTHREAD_PROCESS_SHARED);
	return ret;
}

/**
 * Free a linked list node.
 **/
static void ll_node_free(ll_node_t *ll_node)
{
    //free the lock
    //lock_free(ll_node->lock);
    pthread_spin_destroy(&ll_node->lock);
	XFREE(ll_node);
}

/**
 * Create a new empty linked list.
 **/
ll_t *ll_new()
{
	ll_t *ret;

	XMALLOC(ret, 1);
	ret->head = ll_node_new(-1);
	ret->head->next = ll_node_new(INT_MAX);
	ret->head->next->next = NULL;

	return ret;
}

/**
 * Free a linked list and all its contained nodes.
 **/
void ll_free(ll_t *ll)
{
	ll_node_t *next, *curr = ll->head;
	while (curr) {
		next = curr->next;
		ll_node_free(curr);
		curr = next;
	}
	XFREE(ll);
}

int validate(ll_t *ll, ll_node_t *curr, ll_node_t *next) {
    ll_node_t *node = ll->head;
    while (node->key <= curr->key) {
        if (node == curr)
            return curr->next == next;
        node = node->next;
    }
    
    return 0;
}

int ll_contains(ll_t *ll, int key)
{
    ll_node_t *curr, *next, *head; //# = ll->head;
    head = ll->head;
    while (1) {
        curr = head;
        next = curr->next;

        //traverse until match is found
        while (next->key < key) {
            curr = next;
            next = next->next;
        }
    
        //lock so as to check if list is valid
        pthread_spin_lock(&curr->lock);
        pthread_spin_lock(&next->lock);
        //lock_acquire(curr->lock);
        //lock_acquire(curr->next->lock);
        if (validate(ll,curr,next)) {   
            if (key == next->key) {
                pthread_spin_unlock(&curr->lock);
                pthread_spin_unlock(&next->lock);                
                //lock_release(curr->lock);
                //lock_release(curr->next->lock);    
	            return 1;
            }
            else {
                pthread_spin_unlock(&curr->lock);
                pthread_spin_unlock(&next->lock);
                //lock_release(curr->lock);
                //lock_release(curr->next->lock);
                return 0;    
            } 
        }
        pthread_spin_unlock(&curr->lock);
        pthread_spin_unlock(&next->lock);
//        lock_release(curr->lock);
//        lock_release(curr->next->lock);
    }
}

int ll_add(ll_t *ll, int key)
{
    //int ret = 0;
    ll_node_t *curr, *next, *head;
    head = ll->head;
    ll_node_t *new_node;
    //try until success
    while (1) {

        curr = head;
        next = curr->next;
        
        //traverse list without locks
        while (next->key <= key) {
            curr = next;
            next = next->next;
        }
        //when found the one lock the nodes and check validity
        //lock_acquire(curr->lock);
        pthread_spin_lock(&curr->lock);
        //lock_acquire(next->lock);
        pthread_spin_lock(&next->lock);
        if (validate(ll,curr,next)) {
            if (key != next->key) {
                new_node = ll_node_new(key);
                new_node->next = next;
                curr->next = new_node;
                pthread_spin_unlock(&curr->lock);
                //lock_release(curr->lock);
                pthread_spin_unlock(&next->lock);
                //lock_release(next->lock);
                return 1;
            }
            else {
                pthread_spin_unlock(&curr->lock);
                //lock_release(curr->lock);
                pthread_spin_unlock(&next->lock);
                //lock_release(next->lock);
                return 0;
            }
        }
        pthread_spin_unlock(&curr->lock);
        //lock_release(curr->lock);
        pthread_spin_unlock(&next->lock);
        //lock_release(next->lock);
    }
}

int ll_remove(ll_t *ll, int key)
{
    //int ret = 0;
    //try until success
    ll_node_t *curr, *next, *head;
    head = ll->head;
    while (1) {
        curr = head;
        next = curr->next;

        //traverse list without locks
        while (next->key <= key) {
            if (key == next->key) break;
            curr = next;
            next = next->next;
        }
        /*while (next->key < key) {
            curr =next; next = next->next;

        }*/
        //when found the one, lock the nodes and check validity
        //lock_acquire(curr->lock);
        pthread_spin_lock(&curr->lock);
        //lock_acquire(next->lock);
        pthread_spin_lock(&next->lock);
        if (validate(ll,curr,next)) {
            //if the list is valid, then remove the node
            if (key == next->key) {
                //ret = 1;
                curr->next = next->next;
                //release a locked node???
                pthread_spin_unlock(&curr->lock);
                pthread_spin_unlock(&next->lock);
                //lock_release(curr->lock);
                //lock_release(next->lock);
                //ll_node_free(next);
                return 1;
            }
            else {
                pthread_spin_unlock(&curr->lock);
                //lock_release(curr->lock);
                pthread_spin_unlock(&next->lock);
                //lock_release(next->lock);
                return 0;
            }
        }
        pthread_spin_unlock(&curr->lock);
        //lock_release(curr->lock);
        pthread_spin_unlock(&next->lock);    
        //lock_release(next->lock);
    }
}   

/**
 * Print a linked list.
 **/
void ll_print(ll_t *ll)
{
	ll_node_t *curr = ll->head;
	printf("LIST [");
	while (curr) {
		if (curr->key == INT_MAX)
			printf(" -> MAX");
		else
			printf(" -> %d", curr->key);
		curr = curr->next;
	}
	printf(" ]\n");
}
