#!/bin/bash

## Give the Job a descriptive name
#PBS -N makejob

## Output and error files
#PBS -o logs/makejob.out
#PBS -e logs/makejob.err

## How many machines should we get?
#PBS -l nodes=sandman:ppn=1
## Start 
## Run make in the src folder (modify properly)i
module load openmp
cd /home/parallel/parlab10/a3/z3
make

