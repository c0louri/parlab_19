#include <stdio.h>
#include <stdlib.h> /* rand() */
#include <limits.h>
#include <pthread.h> /* for pthread_spinlock_t */

#include "../common/alloc.h"
#include "ll.h"
#include "lock.h"

typedef struct ll_node {
	int key;
	struct ll_node *next;
    //spinlock for every node
    // lock_t *lock;
    pthread_spinlock_t lock;
    int marked;
} ll_node_t;

struct linked_list {
	ll_node_t *head;
	/* other fields here? */
};

/**
 * Create a new linked list node.
 **/
static ll_node_t *ll_node_new(int key)
{
	ll_node_t *ret;

	XMALLOC(ret, 1);
	ret->key = key;
	ret->next = NULL;
    //initialize the lock
    //ret->lock = lock_init();
    pthread_spin_init(&ret->lock, PTHREAD_PROCESS_SHARED);
    //initialize the marked flag
    ret->marked = 0;
	return ret;
}

/**
 * Free a linked list node.
 **/
static void ll_node_free(ll_node_t *ll_node)
{
	XFREE(ll_node);
}

/**
 * Create a new empty linked list.
 **/
ll_t *ll_new()
{
	ll_t *ret;

	XMALLOC(ret, 1);
	ret->head = ll_node_new(-1);
	ret->head->next = ll_node_new(INT_MAX);
	ret->head->next->next = NULL;

	return ret;
}

/**
 * Free a linked list and all its contained nodes.
 **/
void ll_free(ll_t *ll)
{
	ll_node_t *next, *curr = ll->head;
	while (curr) {
		next = curr->next;
		ll_node_free(curr);
		curr = next;
	}
	XFREE(ll);
}

//traverse the list once without locking
//validate is contained here (logical check - marked flag)
int ll_contains(ll_t *ll, int key)
{
    ll_node_t *curr = ll->head;
    int ret = 0;

    while (curr->key < key) {
        curr = curr->next;
    }
    
    ret = (key == curr->key && !curr->marked);
    return ret;
}

//just checks that curr and next aren't marked and obviously the connection between them
int validate(ll_node_t *curr, ll_node_t *next) {
    return !curr->marked && !next->marked && curr->next->key == next->key;
}

int ll_add(ll_t *ll, int key)
{
    ll_node_t *curr, *next, *head, *new_node;
    head = ll->head;
    //try until success
    while (1) {
        curr = head;
        next = curr->next;

        //traverse list without locks
        while (next->key < key) {
            curr = next;
            next = next->next;
        }
        //when found the one lock the nodes and check validity
        pthread_spin_lock(&curr->lock);
        pthread_spin_lock(&next->lock);
        if (validate(curr,next)) {
            if (key != next->key) {
                new_node = ll_node_new(key);
                new_node->next = next;
                curr->next = new_node;
                pthread_spin_unlock(&curr->lock);
                pthread_spin_unlock(&next->lock);
                return 1;
            }
            else {
                pthread_spin_unlock(&curr->lock);
                pthread_spin_unlock(&next->lock);
                return 0;
            }
        }
        pthread_spin_unlock(&curr->lock);
        pthread_spin_unlock(&next->lock);
    }
}

int ll_remove(ll_t *ll, int key)
{
    ll_node_t *curr, *next, *head;
    head = ll->head;
    //try until success
    while (1) {

        curr = head;
        next = curr->next;

        //traverse list without locks
        while (next->key < key) {
            curr = next;
            next = next->next;
        }
        //when found the one, lock the nodes and check validity
        pthread_spin_lock(&curr->lock);
        pthread_spin_lock(&next->lock);
        if (validate(curr,next)) {
            //if the list is valid, then remove the node
            if (key == next->key) {
                //first delete logically
                next->marked = 1;
                //then delete physically
                curr->next = next->next;
                //finally free the data
                pthread_spin_unlock(&curr->lock);
                pthread_spin_unlock(&next->lock);
                return 1;
            }
            else {
                pthread_spin_unlock(&curr->lock);
                pthread_spin_unlock(&next->lock);
                return 0;
            }
        }
        pthread_spin_unlock(&curr->lock);
        pthread_spin_unlock(&next->lock);
    }
}

/**
 * Print a linked list.
 **/
void ll_print(ll_t *ll)
{
	ll_node_t *curr = ll->head;
	printf("LIST [");
	while (curr) {
		if (curr->key == INT_MAX)
			printf(" -> MAX");
		else
			printf(" -> %d", curr->key);
		curr = curr->next;
	}
	printf(" ]\n");
}
