#include <pthread.h>
#include "lock.h"
#include "../common/alloc.h"

struct lock_struct {
    pthread_spinlock_t sp_lock;
};

lock_t *lock_init()
{
	lock_t *lock;
	XMALLOC(lock, 1);
    int ret;
    ret = pthread_spin_init(&(lock->sp_lock), PTHREAD_PROCESS_PRIVATE);
    if (ret != 0) return NULL;
	else return lock;
}

void lock_free(lock_t *lock)
{
    pthread_spin_destroy(&(lock->sp_lock));
	XFREE(lock);
}

void lock_acquire(lock_t *lock)
{
    pthread_spin_lock(&(lock->sp_lock));  
}

void lock_release(lock_t *lock)
{
    pthread_spin_unlock(&(lock->sp_lock));  
}
