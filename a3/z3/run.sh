#!/bin/bash

## Give the Job a descriptive name
#PBS -N run_z3

## Output and error files
#PBS -o logs/run_z3.out
#PBS -e logs/run_z3.err

## How many machines should we get? 
#PBS -l nodes=sandman:ppn=64

##How long should the job run for?
#PBS -l walltime=00:15:00

## Start 
## Run make in the src folder (modify properly)

cd /home/parallel/parlab10/a3/z3/

for exec in linked_list_lazy linked_list_nb; do
    for size in 8192; do
            ## 1 THREAD
            MT_CONF=0 ./${exec} ${size} 80 10 10 > ./logs/${exec}_${size}_80_10_10.out
            MT_CONF=0 ./${exec} ${size} 20 40 40 > ./logs/${exec}_${size}_20_40_40.out
            ## 2 THREADS
            MT_CONF=0,1 ./${exec} ${size} 80 10 10 >>./logs/${exec}_${size}_80_10_10.out
            MT_CONF=0,1 ./${exec} ${size} 20 40 40 >> ./logs/${exec}_${size}_20_40_40.out
            ## 4 THREADS
            MT_CONF=0,1,2,3 ./${exec} ${size} 80 10 10 >>./logs/${exec}_${size}_80_10_10.out
            MT_CONF=0,1,2,3 ./${exec} ${size} 20 40 40 >> ./logs/${exec}_${size}_20_40_40.out
            ## 8 THREADS
            MT_CONF=0,1,2,3,4,5,6,7 ./${exec} ${size} 80 10 10 >>./logs/${exec}_${size}_80_10_10.out
            MT_CONF=0,1,2,3,4,5,6,7 ./${exec} ${size} 20 40 40 >>./logs/${exec}_${size}_20_40_40.out
            ## 16 THREADS
            MT_CONF=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 ./${exec} ${size} 80 10 10 >> ./logs/${exec}_${size}_80_10_10.out
            MT_CONF=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 ./${exec} ${size} 20 40 40 >> ./logs/${exec}_${size}_20_40_40.out
            ## 32 THREADS
            MT_CONF=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31 ./${exec} ${size} 80 10 10 >>./logs/${exec}_${size}_80_10_10.out
            MT_CONF=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31 ./${exec} ${size} 20 40 40 >>./logs/${exec}_${size}_20_40_40.out
            ## 64 THREADS
            MT_CONF=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63 ./${exec} ${size} 80 10 10 >> ./logs/${exec}_${size}_80_10_10.out
            MT_CONF=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63 ./${exec} ${size} 20 40 40 >> ./logs/${exec}_${size}_20_40_40.out
    done
done
