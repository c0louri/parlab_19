#include <stdio.h>
#include <stdlib.h> /* rand() */
#include <limits.h>
#include <pthread.h> /* for pthread_spinlock_t */

#include "lock.h"
#include "../common/alloc.h"
#include "ll.h"

typedef struct ll_node {
	int key;
	struct ll_node *next;
    //spinlock for every node
    lock_t *lock;
} ll_node_t;

struct linked_list {
	ll_node_t *head;
};

/**
 * Create a new linked list node.
 **/
static ll_node_t *ll_node_new(int key)
{
	ll_node_t *ret;

	XMALLOC(ret, 1);
	ret->key = key;
	ret->next = NULL;
    //initialize the lock
    ret->lock = lock_init();
	return ret;
}

/**
 * Free a linked list node.
 **/
static void ll_node_free(ll_node_t *ll_node)
{
    //free the lock
    lock_free(ll_node->lock);
	XFREE(ll_node);
}

/**
 * Create a new empty linked list.
 **/
ll_t *ll_new()
{
	ll_t *ret;

	XMALLOC(ret, 1);
	ret->head = ll_node_new(-1);
	ret->head->next = ll_node_new(INT_MAX);
	ret->head->next->next = NULL;

	return ret;
}

/**
 * Free a linked list and all its contained nodes.
 **/
void ll_free(ll_t *ll)
{
	ll_node_t *next, *curr = ll->head;
	while (curr) {
		next = curr->next;
		ll_node_free(curr);
		curr = next;
	}
	XFREE(ll);
}

int ll_contains(ll_t *ll, int key)
{
    ll_node_t *curr = ll->head;
    int ret = 0;

    //lock the head
    lock_acquire(curr->lock);

    while (curr->key < key) {
        //lock the next one
        lock_acquire(curr->next->lock);
        //unlock the previous one
        lock_release(curr->lock);
        curr = curr->next;
    }

    ret = (key == curr->key);
    lock_release(curr->lock);

    return ret;
}

int ll_add(ll_t *ll, int key)
{
    int ret = 0;
    ll_node_t *curr, *next;
    ll_node_t *new_node;
    
    //lock head
    curr = ll->head;
    lock_acquire(curr->lock);
    //lock second
    next = curr->next;
    lock_acquire(next->lock);
    
    //traverse the list with hand-over-hand locking
    while (next->key < key) {
        lock_release(curr->lock);
        curr = next;
        next = curr->next;
        lock_acquire(next->lock);
    }

    if (key != next->key) {
        ret = 1;
        new_node = ll_node_new(key);
        new_node->next = next;
        curr->next = new_node;
    }
    //release the nodes
    lock_release(next->lock);
    lock_release(curr->lock);

	return ret;
}

int ll_remove(ll_t *ll, int key)
{
    int ret = 0;
    ll_node_t *curr, *next;
    
    //lock the head
    curr = ll->head;
    lock_acquire(curr->lock);
    //lock the second
    next = curr->next;
    lock_acquire(next->lock);

    //traverse the list with hand-over-hand locking
    while (next->key < key) {
        lock_release(curr->lock);
        curr = next;
        next = curr->next;
        lock_acquire(next->lock);
    }

    if (key == next->key) {
        ret = 1;
        curr->next = next->next;
        lock_release(next->lock);
        lock_release(curr->lock);
        ll_node_free(next);
    }
    else { 
        lock_release(next->lock);
        lock_release(curr->lock);
    }

	return ret;
}

/**
 * Print a linked list.
 **/
void ll_print(ll_t *ll)
{
	ll_node_t *curr = ll->head;
	printf("LIST [");
	while (curr) {
		if (curr->key == INT_MAX)
			printf(" -> MAX");
		else
			printf(" -> %d", curr->key);
		curr = curr->next;
	}
	printf(" ]\n");
}
