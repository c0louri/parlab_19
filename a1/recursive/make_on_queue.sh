#!/bin/bash

## Give the Job a descriptive name
#PBS -N makejob_sr

## Output and error files
#PBS -o logs_parlab/makejob.out
#PBS -e logs_parlab/makejob.err

## How many machines should we get?
#PBS -l nodes=1

## Start 
## Run make in the src folder (modify properly)i
module load openmp
cd /home/parallel/parlab10/a1/FW/recursive
##make clean
make

