#!/bin/bash

## Give the Job a descriptive name
#PBS -N run_omp_fw_sr

## Output and error files
#PBS -o logs_parlab/run_omp_sr0.out
#PBS -e logs_parlab/run_omp_sr0.err

## How many machines should we get? 
#PBS -l nodes=1:ppn=8

##How long should the job run for?
#PBS -l walltime=00:10:00

## Start 
## Run make in the src folder (modify properly)

module load openmp
cd /home/parallel/parlab10/a1/FW/recursive
export OMP_NUM_THREADS=8
./fw_sr 128 16
./fw_sr6 128 16

