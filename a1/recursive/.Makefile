.phony: all clean

# all: fw_sr7 fw_sr8 fw_sr fw_sr6 fw_sr_serial
all: fw_sr9
CC=gcc
CFLAGS= -lm -Wall -O3 -Wno-unused-variable -fopenmp

HDEPS+=%.h

OBJS=util.o

fw_sr7: $(OBJS) fw_sr7.c 
	$(CC) $(OBJS) fw_sr7.c -o fw_sr7 $(CFLAGS)

fw_sr8: $(OBJS) fw_sr8.c 
	$(CC) $(OBJS) fw_sr8.c -o fw_sr8 $(CFLAGS)


fw_sr9: $(OBJS) fw_sr9.c 
	$(CC) $(OBJS) fw_sr9.c -o fw_sr9 $(CFLAGS)

fw_sr6: $(OBJS) fw_sr6.c 
	$(CC) $(OBJS) fw_sr6.c -o fw_sr6 $(CFLAGS)

fw_sr_serial: $(OBJS) fw_sr_serial.c 
	$(CC) $(OBJS) fw_sr_serial.c -o fw_sr_serial $(CFLAGS)

%.o: %.c $(HDEPS)
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f fw_sr9 fw_sr_serial

