#!/bin/bash

## Give the Job a descriptive name
#PBS -N run_omp_fw_sr

## Output and error files
#PBS -o logs/run_omp_sr3.out
#PBS -e logs/run_omp_sr3.err

## How many machines should we get? 
#PBS -l nodes=sandman:ppn=64
##How long should the job run for?
#PBS -l walltime=00:30:00

## Start 
## Run make in the src folder (modify properly)
##3rd try
EXECP="fw_sr3"
CORES="1 2 4 8 16 32 64"
SIZES="1024 2048 4048"
B="128"
STATS="logs/perf2_all_128_$EXECP"

module load openmp
cd /home/parallel/parlab10/a1/FW/recursive

echo "measurements for $EXECP" > $STATS
echo "-------------------------" >> $STATS

for size in $SIZES; do
	echo "no affinity" >> $STATS
	for cores in $CORES; do
		export OMP_NUM_THREADS=$cores
		for b in $B; do
			./$EXECP $size $b >> $STATS
		done
	done
	echo "-------------------------" >> $STATS

	echo "circulating sockets" >> $STATS
	export OMP_PLACES=sockets
	for cores in $CORES; do
		export OMP_NUM_THREADS=$cores
		for b in $B; do
			./$EXECP $size $b >> $STATS
		done
	done
	echo "-------------------------" >> $STATS

	echo "close affinity" >> $STATS
	export OMP_PLACES=cores
	export OMP_PROC_BIND=close
	for cores in $CORES; do
		export OMP_NUM_THREADS=$cores
		for b in $B; do
			./$EXECP $size $b >> $STATS
		done
	done
	echo "-------------------------" >> $STATS

	echo "spread affinity" >> $STATS
	export OMP_PLACES=cores
	export OMP_PROC_BIND=spread
	for cores in $CORES; do
		export OMP_NUM_THREADS=$cores
		for b in $B; do
			./$EXECP $size $b >> $STATS
		done
	done
done
