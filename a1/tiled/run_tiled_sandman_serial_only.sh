#!/bin/bash

## Give the Job a descriptive name
#PBS -N run_omp_fw_tiled

## Output and error files
#PBS -o logs/run_omp_tiled_serial.out
#PBS -e logs/run_omp_tiled_serial.err

## How many machines should we get? 
#PBS -l nodes=sandman:ppn=64
##PBS -l nodes=1:ppn=8

##How long should the job run for?
#PBS -l walltime=00:30:00

## Start 
## Run make in the src folder (modify properly)

EXECS="fw_tiled"
CORES="1"
SIZES="1024 2048 4096"
B="16 32 64 128 256 512"
STATS="logs/perf_serial_$EXECS"

module load openmp
cd /home/parallel/parlab10/a1/FW/tiled

echo "measurements for $EXECS" > $STATS
echo "-------------------------" >> $STATS

for size in $SIZES; do
	echo "-------------------------" >> $STATS
	echo "serial" >> $STATS
	for b in $B; do
		./$EXECS $size $b >> $STATS
	done
	echo "-------------------------" >> $STATS
done


