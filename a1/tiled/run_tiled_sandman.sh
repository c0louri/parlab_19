#!/bin/bash

## Give the Job a descriptive name
#PBS -N run_omp_fw_tiled1

## Output and error files
#PBS -o logs/run_omp_tiled1.out
#PBS -e logs/run_omp_tiled1.err

## How many machines should we get? 
#PBS -l nodes=sandman:ppn=64

##How long should the job run for?
#PBS -l walltime=00:20:00

## Start 
## Run make in the src folder (modify properly)

EXECP="fw_tiled_for"
CORES="1 2 4 8 16 32 64"
SIZES="4096"
B="64 128 256"
STATS="logs/perf000_$EXECP"

module load openmp
cd /home/parallel/parlab10/a1/FW/tiled

echo "measurements for $EXECP" > $STATS
echo "-------------------------" >> $STATS

for size in $SIZES; do
	echo "close affinity" >> $STATS
	export OMP_PLACES=cores
	export OMP_PROC_BIND=close
	for cores in $CORES; do
		export OMP_NUM_THREADS=$cores
		for b in $B; do
			./$EXECP $size $b >> $STATS
		done
	done
	echo "-------------------------" >> $STATS

	echo "spread affinity" >> $STATS
	export OMP_PLACES=cores
	export OMP_PROC_BIND=spread
	for cores in $CORES; do
		export OMP_NUM_THREADS=$cores
		for b in $B; do
			./$EXECP $size $b >> $STATS
		done
	done
done


