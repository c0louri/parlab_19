#!/bin/bash

## Give the Job a descriptive name
#PBS -N run_omp_fw_tiled

## Output and error files
#PBS -o logs_parlab/run_omp_tiled_for_comp.out
#PBS -e logs_parlab/run_omp_tiled_for_comp.err

## How many machines should we get? 
#PBS -l nodes=1:ppn=8

##How long should the job run for?
#PBS -l walltime=00:10:00

## Start 
## Run make in the src folder (modify properly)

module load openmp
cd /home/parallel/parlab10/a1/FW/tiled
export OMP_NUM_THREADS=8
./fw_tiled 128 8
./fw_tiled_for 128 8
./fw_tiled_for2 128 8
./fw_tiled_for2_5 128 8
./fw_tiled_for3 128 8
./fw_tiled_for4 128 8
./fw_tiled_for4_5 128 8
./fw_tiled_for5 128 8
