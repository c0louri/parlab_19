#!/bin/bash

## Give the Job a descriptive name
#PBS -N run_omp_fw

## Output and error files
#PBS -o logs/run_omp_serial.out
#PBS -e logs/run_omp_serial.err

## How many machines should we get? 
#PBS -l nodes=sandman:ppn=1

##How long should the job run for?
#PBS -l walltime=00:30:00

## Start 
## Run make in the src folder (modify properly)

EXECS="fw"
SIZES="1024 2048 4096"
STATS="logs/perf_serial_$EXECS"

cd /home/parallel/parlab10/a1/FW/classic

echo "measurements for serial_$EXECS" > $STATS
echo "-------------------------" >> $STATS

for size in $SIZES; do
	echo "-------------------------" >> $STATS
	echo "serial" >> $STATS
	./$EXECS $size >> $STATS
	echo "-------------------------" >> $STATS
done


