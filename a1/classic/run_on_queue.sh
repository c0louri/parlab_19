#!/bin/bash

## Give the Job a descriptive name
#PBS -N run_omp_fw

## Output and error files
#PBS -o logs_parlab/fw_i_st-run_omp.out
#PBS -e logs_parlab/fw_i_st-run_omp.err

## How many machines should we get? 
#PBS -l nodes=1:ppn=8

##How long should the job run for?
#PBS -l walltime=00:30:00

## Start 
## Run make in the src folder (modify properly)

module load openmp
cd /home/parallel/parlab10/a1/FW/classic
export OMP_NUM_THREADS=8
./fw_ij 4096
