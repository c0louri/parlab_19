#!/bin/bash

## Give the Job a descriptive name
#PBS -N run_omp_fw

## Output and error files
#PBS -o logs/run_omp_i.out
#PBS -e logs/run_omp_i.err

## How many machines should we get? 
#PBS -l nodes=sandman:ppn=64

##How long should the job run for?
#PBS -l walltime=00:50:00

## Start 
## Run make in the src folder (modify properly)

EXECP="fw_ij"
CORES="1 2 4 8 16 32 64"
SIZES="1024 2048 4096"
STATS="logs/perf_fw_i_parallelfor"

module load openmp
cd /home/parallel/parlab10/a1/FW/classic

echo "measurements for $EXECP" > $STATS
echo "-------------------------" >> $STATS

for size in $SIZES; do
        echo "-------------------------" >> $STATS

	echo "no affinity" >> $STATS
	for cores in $CORES; do
		export OMP_NUM_THREADS=$cores
		./$EXECP $size >> $STATS
	done
	echo "-------------------------" >> $STATS

	echo "circulating sockets" >> $STATS
	export OMP_PLACES=sockets
	for cores in $CORES; do
		export OMP_NUM_THREADS=$cores
		./$EXECP $size >> $STATS
	done
	echo "-------------------------" >> $STATS

	echo "close affinity" >> $STATS
	export OMP_PLACES=cores
	export OMP_PROC_BIND=close
	for cores in $CORES; do
		export OMP_NUM_THREADS=$cores
		./$EXECP $size >> $STATS
	done
	echo "-------------------------" >> $STATS

	echo "spread affinity" >> $STATS
	export OMP_PLACES=cores
	export OMP_PROC_BIND=spread
	for cores in $CORES; do
		export OMP_NUM_THREADS=$cores
		./$EXECP $size >> $STATS
	done
done


