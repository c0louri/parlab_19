#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include "mpi.h"
#include "utils.h"

void Jacobi(double ** u_previous, double ** u_current, int X_min, int X_max, int Y_min, int Y_max) {
    int i,j;
    for (i=X_min;i<=X_max;i++)
        for (j=Y_min;j<=Y_max;j++)
            u_current[i][j]=(u_previous[i-1][j]+u_previous[i+1][j]+u_previous[i][j-1]+u_previous[i][j+1])/4.0;
}

int local_converge(double ** u_previous, double ** u_current, int Xmin, int Xmax, int Ymin, int Ymax) {
    int i,j;
    for (i=Xmin;i<=Xmax;i++)
        for (j=Ymin;j<=Ymax;j++)
            if (fabs(u_current[i][j]-u_previous[i][j])>e) return 0;
    return 1;
}

void copy_col_to_u_prev(double **u, double *buf, int col, int dim) {
    int i;
    for(i=1; i<=dim; i++)
        u[i][col] = buf[i-1];
} 

void copy_row_to_u_prev(double **u, double *buf, int row, int dim) {
    int i;
    for(i=1; i<=dim; i++)
        u[row][i] = buf[i-1];
}

void print_buf(double *buf, int dim){
    int i;
    for(i=0; i<dim; i++)
        printf("%lf ", buf[i]);
    printf("\n");
}

void get_col_from_u_curr(double **u, double *buf, int col, int dim) {
    int i;
    for(i=1; i<=dim; i++)
        buf[i-1] = u[i][col];
}

void get_row_from_u_curr(double **u, double *buf, int row, int dim) {
    int i;
    for(i=1; i<=dim; i++)
        buf[i-1] = u[row][i];
}


int main(int argc, char ** argv) {
    int rank,size;
    int global[2],local[2]; //global matrix dimensions and local matrix dimensions (2D-domain, 2D-subdomain)
    int global_padded[2];   //padded global matrix dimensions (if padding is not needed, global_padded=global)
    int grid[2];            //processor grid dimensions
    int i,j,t;
    int global_converged=0,converged=0; //flags for convergence, global and per process
    MPI_Datatype dummy;     //dummy datatype used to align user-defined datatypes in memory
    double omega;           //relaxation factor - useless for Jacobi

    struct timeval tts,ttf,tcs,tcf,tbc,tac;  //Timers: total-> tts,ttf, computation -> tcs,tcf, converge->tbc, tac
    double ttotal=0,tcomp=0,tconv=0,total_time,comp_time, conv_time;
    
    double ** U, ** u_current, ** u_previous, ** swap; //Global matrix, local current and previous matrices, pointer to swap between current and previous
    

    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    //----Read 2D-domain dimensions and process grid dimensions from stdin----//

    if (argc!=5) {
        fprintf(stderr,"Usage: mpirun .... ./exec X Y Px Py");
        exit(-1);
    }
    else {
        global[0]=atoi(argv[1]);
        global[1]=atoi(argv[2]);
        grid[0]=atoi(argv[3]);
        grid[1]=atoi(argv[4]);
    }

    //----Create 2D-cartesian communicator----//
    //----Usage of the cartesian communicator is optional----//

    MPI_Comm CART_COMM;         //CART_COMM: the new 2D-cartesian communicator
    int periods[2]={0,0};       //periods={0,0}: the 2D-grid is non-periodic
    int rank_grid[2];           //rank_grid: the position of each process on the new communicator
        
    MPI_Cart_create(MPI_COMM_WORLD,2,grid,periods,0,&CART_COMM);    //communicator creation
    MPI_Cart_coords(CART_COMM,rank,2,rank_grid);                   //rank mapping on the new communicator

    //----Compute local 2D-subdomain dimensions----//
    //----Test if the 2D-domain can be equally distributed to all processes----//
    //----If not, pad 2D-domain----//
    
    for (i=0;i<2;i++) {
        if (global[i]%grid[i]==0) {
            local[i]=global[i]/grid[i];
            global_padded[i]=global[i];
        }
        else {
            local[i]=(global[i]/grid[i])+1;
            global_padded[i]=local[i]*grid[i];
        }
    }

    //----Allocate global 2D-domain and initialize boundary values----//
    //----Rank 0 holds the global 2D-domain----//
    if (rank==0) {
        U=allocate2d(global_padded[0],global_padded[1]);   
        init2d(U,global[0],global[1]);
    }

    //----Allocate local 2D-subdomains u_current, u_previous----//
    //----Add a row/column on each size for ghost cells----//

    u_previous=allocate2d(local[0]+2,local[1]+2);
    u_current=allocate2d(local[0]+2,local[1]+2);   
       
    //----Distribute global 2D-domain from rank 0 to all processes----//        
    //----Appropriate datatypes are defined here----//
    /*****The usage of datatypes is optional*****/

    //----Datatype definition for the 2D-subdomain on the global matrix----//
    MPI_Datatype global_block;
    MPI_Type_vector(local[0],local[1],global_padded[1],MPI_DOUBLE,&dummy);
    MPI_Type_create_resized(dummy,0,sizeof(double),&global_block);
    MPI_Type_commit(&global_block);

    //----Datatype definition for the 2D-subdomain on the local matrix----//
    MPI_Datatype local_block;
    MPI_Type_vector(local[0],local[1],local[1]+2,MPI_DOUBLE,&dummy);
    MPI_Type_create_resized(dummy,0,sizeof(double),&local_block);
    MPI_Type_commit(&local_block);

    //----Rank 0 defines positions and counts of local blocks (2D-subdomains) on global matrix----//
    int * scatteroffset, * scattercounts;
    if (rank==0) {
        scatteroffset=(int*)malloc(size*sizeof(int));
        scattercounts=(int*)malloc(size*sizeof(int));
        for (i=0;i<grid[0];i++)
            for (j=0;j<grid[1];j++) {
                scattercounts[i*grid[1]+j]=1;
                scatteroffset[i*grid[1]+j]=(local[0]*local[1]*grid[1]*i+local[1]*j);
            }
    }
    //----Rank 0 scatters the global matrix (U)----//
    MPI_Scatterv(&U[0][0], scattercounts, scatteroffset, global_block, &u_previous[1][1], 1, local_block, 0, MPI_COMM_WORLD);
    // copy copy contents of u_current to u_previous at the right spots
    for (i=1; i<=local[0]; i++)
        for(j=1; j<=local[1]; j++)
            u_current[i][j] = u_previous[i][j];

    if (rank==0) free2d(U);
     
    //----Define datatypes or allocate buffers for message passing----//
    double * to_north_buf, * to_south_buf, * to_east_buf, * to_west_buf;
    to_north_buf = (double*)calloc(local[1], sizeof(double));
    to_south_buf = (double*)calloc(local[1], sizeof(double));
    to_east_buf = (double*)calloc(local[0], sizeof(double));
    to_west_buf = (double*)calloc(local[0], sizeof(double));
    //
    double * from_north_buf, * from_south_buf, * from_west_buf, * from_east_buf;
    from_north_buf = (double*)calloc(local[1], sizeof(double));
    from_south_buf = (double*)calloc(local[1], sizeof(double));
    from_east_buf = (double*)calloc(local[0], sizeof(double));
    from_west_buf = (double*)calloc(local[0], sizeof(double));
    //************************************//
    //----Find the 4 neighbors with which a process exchanges messages----//
    int north, south, east, west;
    // north -> rank - Py (grid[1])
    if (rank - grid[1] >= 0) north = rank - grid[1];
    else north = -1;
    // south -> rank + Py (grid[1])
    if (rank + grid[1] < size) south = rank + grid[1];
    else south = -1;
    // west
    if ((int)(rank/grid[1]) == (int)((rank-1)/grid[1])) west = rank - 1;
    else west = -1;
    // east
    if ((int)(rank/grid[1]) == (int)((rank+1)/grid[1])) east = rank + 1;
    else east = -1;
    /*Make sure you handle non-existing neighbors appropriately*/
    //************************************//
    //---Define the iteration ranges per process-----//
    int i_min,i_max,j_min,j_max;
    //mono ta terma deksia kai terma katw mporei na einai me axrhsta stoixeia
    // no north neighbor -> j_min = 2, else 1
    if (north < 0) i_min = 2;
    else i_min = 1;
    // no west neighbor -> i_min = 2, else 1   
    if (west < 0) j_min = 2;
    else j_min = 1;
    // for south_neighbor
    if (south > 0) i_max = local[0];
    else {
        int pad_rows = global_padded[0] - global[0];
        i_max = local[0] - 1 - pad_rows;
    }
    // for east_neighbor
    if (east > 0) j_max = local[1];
    else {
        int pad_cols = global_padded[1] - global[1];
        j_max = local[1] - 1 - pad_cols;
    }
    // initialize counter and array of requests for non-blocking Send
    int cnt = 0;
    MPI_Request s_req[4];
    int cnt_rec = 0;
    MPI_Request r_req[4];
    //----Computational core----//
    gettimeofday(&tts, NULL);
    #ifdef TEST_CONV
    for (t=0;t<T && !global_converged;t++) {
    #endif
    #ifndef TEST_CONV
    #undef T
    #define T 256
    for (t=0;t<T;t++) {
    #endif
        swap = u_previous;
        u_previous = u_current;
        u_current = swap;
        /* Communicate */
        /* Wait for non-blocking Sends of the previous iteration to complete */
        MPI_Waitall(cnt, s_req, MPI_STATUSES_IGNORE);
        cnt = 0;
        /* Firtly, send */
        if (north >= 0) {
            get_row_from_u_curr(u_previous, to_north_buf, i_min, local[1]);
            MPI_Isend(to_north_buf, local[1], MPI_DOUBLE, north, rank, MPI_COMM_WORLD, &s_req[cnt++]);
        }
        if (south >= 0) {
            get_row_from_u_curr(u_previous, to_south_buf, i_max, local[1]);
            MPI_Isend(to_south_buf, local[1], MPI_DOUBLE, south, rank, MPI_COMM_WORLD, &s_req[cnt++]);
        }
        if (west >= 0) {
            get_col_from_u_curr(u_previous, to_west_buf, j_min, local[0]);
            MPI_Isend(to_west_buf, local[0], MPI_DOUBLE, west, rank, MPI_COMM_WORLD, &s_req[cnt++]);
        }
        if (east >= 0) {
            get_col_from_u_curr(u_previous, to_east_buf, j_max, local[0]);
            MPI_Isend(to_east_buf, local[0], MPI_DOUBLE, east, rank, MPI_COMM_WORLD, &s_req[cnt++]);
        }
        cnt_rec = 0;
        /* Secondly, receive (blocking) */
        if (south >= 0)
            MPI_Irecv(from_south_buf, local[1], MPI_DOUBLE, south, south, MPI_COMM_WORLD, &r_req[cnt_rec++]);
        if (north >= 0)
            MPI_Irecv(from_north_buf, local[1], MPI_DOUBLE, north, north, MPI_COMM_WORLD, &r_req[cnt_rec++]);
        if (east >= 0)
            MPI_Irecv(from_east_buf, local[0], MPI_DOUBLE, east, east, MPI_COMM_WORLD, &r_req[cnt_rec++]);
        if (west >= 0)
            MPI_Irecv(from_west_buf, local[0], MPI_DOUBLE, west, west, MPI_COMM_WORLD, &r_req[cnt_rec++]);
        /* Wait for non-blocking receives */
        MPI_Waitall(cnt_rec, r_req, MPI_STATUSES_IGNORE);
        /* store new data from neighbors */
        if (south >= 0)
            copy_row_to_u_prev(u_previous, from_south_buf, i_max+1, local[1]);
        if (north >= 0)
            copy_row_to_u_prev(u_previous, from_north_buf, i_min-1, local[1]);
        if (east >= 0)
            copy_col_to_u_prev(u_previous, from_east_buf, j_max+1, local[0]);
        if (west >= 0)
            copy_col_to_u_prev(u_previous, from_west_buf, j_min-1, local[0]);
        /* save time after receiving adns senting of data */
        gettimeofday(&tcs, NULL);
        /* Compute */
        Jacobi(u_previous, u_current, i_min, i_max, j_min, j_max);
        gettimeofday(&tcf, NULL);
        tcomp += (tcf.tv_sec-tcs.tv_sec)+(tcf.tv_usec-tcs.tv_usec)*0.000001; 
        /*Add appropriate timers for converge*/
        
        #ifdef TEST_CONV
        gettimeofday(&tbc, NULL);
        if (t%C==0) {
            MPI_Barrier(MPI_COMM_WORLD);
            /*Test convergence*/
            converged = local_converge(u_previous, u_current, i_min, i_max, j_min, j_max);
            MPI_Allreduce(&converged, &global_converged, 1, MPI_INT, MPI_PROD, MPI_COMM_WORLD);
            gettimeofday(&tac, NULL);
            tconv += ((tac.tv_sec-tbc.tv_sec)+(tac.tv_usec-tbc.tv_usec)*0.000001);       
        }
        #endif

    }
    /* end of computational core */
    gettimeofday(&ttf,NULL);

    ttotal=(ttf.tv_sec-tts.tv_sec)+(ttf.tv_usec-tts.tv_usec)*0.000001;
    
    MPI_Reduce(&tconv,&conv_time,1,MPI_DOUBLE,MPI_MAX,0,MPI_COMM_WORLD);
    MPI_Reduce(&ttotal,&total_time,1,MPI_DOUBLE,MPI_MAX,0,MPI_COMM_WORLD);
    MPI_Reduce(&tcomp,&comp_time,1,MPI_DOUBLE,MPI_MAX,0,MPI_COMM_WORLD);


    //----Rank 0 gathers local matrices back to the global matrix----//
    if (rank==0) {
            U=allocate2d(global_padded[0],global_padded[1]);
    }
    MPI_Gatherv(&u_current[1][1], 1, local_block, &U[0][0], scattercounts, scatteroffset, global_block, 0, MPI_COMM_WORLD); 
    //************************************//

    //----Printing results----//
    if (rank==0) {
        #ifdef TEST_CONV
            // to print allso Conv time    
            printf("Jacobi X %d Y %d Px %d Py %d Iter %d ConvTime %lf ComputationTime %lf TotalTime %lf midpoint %lf\n",global[0],global[1],grid[0],grid[1],t,conv_time,comp_time,total_time,U[global[0]/2][global[1]/2]);
        #endif
        #ifndef TEST_CONV
            printf("Jacobi X %d Y %d Px %d Py %d Iter %d ComputationTime %lf TotalTime %lf midpoint %lf\n",global[0],global[1],grid[0],grid[1],t,comp_time,total_time,U[global[0]/2][global[1]/2]);
        #endif
        // print2d(U, global[0], global[1]);
        #ifdef PRINT_RESULTS
            char * s=malloc(50*sizeof(char));
            sprintf(s,"resJacobiMPI_%dx%d_%dx%d",global[0],global[1],grid[0],grid[1]);
            fprint2d(s,U,global[0],global[1]);
            free(s);
        #endif
    }
    MPI_Finalize();
    return 0;
}
