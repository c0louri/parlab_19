#!/bin/bash
## Give the Job a descriptive name
#PBS -N testjob
## Output and error files
#PBS -o logs/gssor_test.out
#PBS -e logs/gssor_test.err
## Limit memory, runtime etc.
#PBS -l walltime=00:20:00
## How many nodes:processors_per_node should we get?
#PBS -l nodes=2:ppn=8
## Start 
##echo "PBS_NODEFILE = $PBS_NODEFILE"
##cat $PBS_NODEFILE
## Load appropriate module
module load openmpi/1.8.3
## Run the job (use full paths to make sure we execute the correct thing) 
##mpirun -np 64 --map-by node --mca btl self,tcp /home/parallel/parlab10/a2/mpi/jac_mpi 128 128 4 4
cd /home/parallel/parlab10/a2/mpi/
mpirun -np 16 --map-by node --mca btl self,tcp /home/parallel/parlab10/a2/mpi/gssor_mpi_ind 128 128 4 4
