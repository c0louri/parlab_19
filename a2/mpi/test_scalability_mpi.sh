#!/bin/bash

## Give the Job a descriptive name
#PBS -N testjob

## Output and error files
#PBS -o test_scalability_mpi1.out
#PBS -e test_scalability_mpi1.err

## Limit memory, runtime etc.
#PBS -l walltime=02:00:00
## How many nodes:processors_per_node should we get?
## Run on parlab
#PBS -l nodes=8:ppn=8

## Start 
##echo "PBS_NODEFILE = $PBS_NODEFILE"
##cat $PBS_NODEFILE

## Run the job (use full paths to make sure we execute the correct thing) 
## NOTE: Fix the path to show to your executable! 

module load openmpi/1.8.3
cd /home/parallel/parlab10/a2/mpi/
## NOTE: Fix the names of your executables

for size in 2048 4096 6144
do
    for run in first_run sec_run thr_run
	do
        for execfile in gssor_mpi
	    do
    		mpirun  -np 1 --map-by node --mca btl self,tcp ${execfile} ${size} ${size} 1 1 >>./logs/${run}/ScalabilityResultsMPI_${execfile}_${size}
	    	mpirun  -np 2 --map-by node --mca btl self,tcp ${execfile} ${size} ${size} 2 1 >>./logs/${run}/ScalabilityResultsMPI_${execfile}_${size}
		    mpirun  -np 4 --map-by node --mca btl self,tcp ${execfile} ${size} ${size} 2 2 >>./logs/${run}/ScalabilityResultsMPI_${execfile}_${size}
		    mpirun  -np 8 --map-by node --mca btl self,tcp ${execfile} ${size} ${size} 4 2 >>./logs/${run}/ScalabilityResultsMPI_${execfile}_${size}
		    mpirun  -np 16 --map-by node --mca btl self,tcp ${execfile} ${size} ${size} 4 4 >>./logs/${run}/ScalabilityResultsMPI_${execfile}_${size}
		    mpirun  -np 32 --map-by node --mca btl self,tcp ${execfile} ${size} ${size} 8 4 >>./logs/${run}/ScalabilityResultsMPI_${execfile}_${size}
		    mpirun  -np 64 --map-by node --mca btl self,tcp ${execfile} ${size} ${size} 8 8 >>./logs/${run}/ScalabilityResultsMPI_${execfile}_${size}
	    done
    done
done

## Make sure you disable convergence testing and printing
